import express = require("express");
import bodyParser = require('body-parser');
import { exit } from "process";
import { Sequelize, DataType, DataTypes } from "sequelize";
const cors = require("cors");

const sequelize = new Sequelize("data", "root", "pass", {
  host: 'db',
  port: 3306,
  dialect: "mysql",
});

const app = express();
const port = 3000;

const model = sequelize.define("form", {
  name: {
    type: DataTypes.STRING,
  },
  score: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  }
}, {
  tableName: 'Leaderboard'
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello from server!");
});

app.get("/leaderboard", (req, res) => {
  //res.setHeader('Access-Control-Allow-Origin', '*');
  model.findAll({
    order: [
      ['score', 'DESC'],
      ['createdAt', 'ASC']
    ]
  }).then(result => {
    res.send(result);
  }).catch(err => {
    console.log(err); 
    res.sendStatus(501);
  });
});

app.post("/submit", (req, res) => {
  // res.setHeader('Access-Control-Allow-Origin', '*');
  // res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
  model.create({name: req.body.name, score: req.body.score}).then(result => {
    res.send(result);
  }).catch(err => {
    console.log(err);
    res.sendStatus(501);
  })
})

app.listen(port, async () => {
  console.log(`Example app listening at http://localhost:${port}`);
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
    model.sync();
  } catch (error) {
    console.error("Unable to connect to the database:", error);
    exit(1);
  }
});
