describe("Test link", () => {
    it("should visit form page", () => {
        cy.visit("/");
    });
    it("should stay first page", () => {
        cy.contains('Questionnaire').click();
        cy.url().should('include', '/');
    });
    it("should go on leaderboard", () => {
        cy.contains('Classement').click();
        cy.url().should('include', '/leaderboard');
    });
    it("should go back to first page", () => {
        cy.contains('Questionnaire').click();
        cy.url().should('include', '/');
    });
});