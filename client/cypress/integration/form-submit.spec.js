describe("omit some fields", () => {
    it("should not submit", () => {
        cy.visit("/");
        cy.contains('Soumettre').should('be.disabled');
    });
    it("answer a question and should not submit", () => {
        cy.get('mat-radio-button').contains('Lundi').click();
        cy.contains('Soumettre').should('be.disabled');
    });
    it("answer a few question and should not submit", () => {
        cy.get('mat-radio-button').contains('Lundi').click();
        cy.get('mat-horizontal-stepper').contains('Question n°4').click();
        cy.get('mat-radio-button').contains('Un crapaud').click();
        cy.get('mat-horizontal-stepper').contains('Question n°8').click();
        cy.get('mat-radio-button').contains('Hypnos').click();
        cy.contains('Soumettre').should('be.disabled');
    });
    it("answer name and should not submit", () => {
        cy.get('mat-horizontal-stepper').contains('Fin').click();
        cy.get('mat-form-field').type('Test');
        cy.contains('Soumettre').should('be.disabled');
    });
});

describe("Form passed", () => {
    it("should submit", () => {
        cy.visit("/");
        cy.contains('Soumettre').should('be.disabled');
        cy.get('mat-radio-button').contains('Lundi').click();
        cy.get('mat-horizontal-stepper').contains('Question n°2').click();
        cy.get('mat-radio-button').contains('La paonne').click();
        cy.get('mat-horizontal-stepper').contains('Question n°3').click();
        cy.get('mat-radio-button').contains('Une ceinture').click();
        cy.get('mat-horizontal-stepper').contains('Question n°4').click();
        cy.get('mat-radio-button').contains('Un crapaud').click();
        cy.get('mat-horizontal-stepper').contains('Question n°5').click();
        cy.get('mat-radio-button').contains('Un piano').click();
        cy.get('mat-horizontal-stepper').contains('Question n°6').click();
        cy.get('mat-radio-button').contains('Fuschia').click();
        cy.get('mat-horizontal-stepper').contains('Question n°7').click();
        cy.get('mat-radio-button').contains('1ere place').click();
        cy.get('mat-horizontal-stepper').contains('Question n°8').click();
        cy.get('mat-radio-button').contains('Thalassa').click();
        cy.get('mat-horizontal-stepper').contains('Fin').click();
        cy.get('mat-form-field').type('Cypress-1');
        cy.contains('Soumettre').click();
        cy.url().should('include', '/leaderboard');
    });
    it("should modify answer and submit", () => {
        cy.visit("/");
        cy.contains('Soumettre').should('be.disabled');
        cy.get('mat-radio-button').contains('Lundi').click();
        cy.get('mat-horizontal-stepper').contains('Question n°2').click();
        cy.get('mat-radio-button').contains('La paonne').click();
        cy.get('mat-horizontal-stepper').contains('Question n°3').click();
        cy.get('mat-radio-button').contains('Une ceinture').click();
        cy.get('mat-horizontal-stepper').contains('Question n°4').click();
        cy.get('mat-radio-button').contains('Un crapaud').click();
        cy.get('mat-horizontal-stepper').contains('Question n°5').click();
        cy.get('mat-radio-button').contains('Un piano').click();
        cy.get('mat-horizontal-stepper').contains('Question n°6').click();
        cy.get('mat-radio-button').contains('Fuschia').click();
        cy.get('mat-horizontal-stepper').contains('Question n°7').click();
        cy.get('mat-radio-button').contains('1ere place').click();
        cy.get('mat-horizontal-stepper').contains('Question n°8').click();
        cy.get('mat-radio-button').contains('Thalassa').click();
        cy.get('mat-horizontal-stepper').contains('Question n°3').click();
        cy.get('mat-radio-button').contains('Des sandales').click();
        cy.get('mat-horizontal-stepper').contains('Question n°6').click();
        cy.get('mat-radio-button').contains('Fuchsia').click();
        cy.get('mat-horizontal-stepper').contains('Question n°8').click();
        cy.get('mat-radio-button').contains('Synthétos').click();
        cy.get('mat-horizontal-stepper').contains('Fin').click();
        cy.get('mat-form-field').type('Cypress-2');
        cy.contains('Soumettre').click();
        cy.url().should('include', '/leaderboard');
    });
    it("should submit perfect answers", () => {
        cy.visit("/");
        cy.contains('Soumettre').should('be.disabled');
        cy.get('mat-radio-button').contains('Jeudi').click();
        cy.get('mat-horizontal-stepper').contains('Question n°2').click();
        cy.get('mat-radio-button').contains('La kangouroute').click();
        cy.get('mat-horizontal-stepper').contains('Question n°3').click();
        cy.get('mat-radio-button').contains('Des sandales').click();
        cy.get('mat-horizontal-stepper').contains('Question n°4').click();
        cy.get('mat-radio-button').contains('Une courgette').click();
        cy.get('mat-horizontal-stepper').contains('Question n°5').click();
        cy.get('mat-radio-button').contains('Un violon').click();
        cy.get('mat-horizontal-stepper').contains('Question n°6').click();
        cy.get('mat-radio-button').contains('Fuchsia').click();
        cy.get('mat-horizontal-stepper').contains('Question n°7').click();
        cy.get('mat-radio-button').contains('3e place').click();
        cy.get('mat-horizontal-stepper').contains('Question n°8').click();
        cy.get('mat-radio-button').contains('Synthétos').click();
        cy.get('mat-horizontal-stepper').contains('Fin').click();
        cy.get('mat-form-field').type('Cypress-Perfect');
        cy.contains('Soumettre').click();
        cy.url().should('include', '/leaderboard');
    });
    it("should verify first submit", () => {
        cy.visit('/leaderboard');
        cy.get('tr.mat-row.cdk-row').contains('Cypress-1').get('td.mat-cell.cdk-cell.cdk-column-score.mat-column-score').should('have.value', '');
    });
    it("should verify second submit", () => {
        cy.visit('/leaderboard');
        cy.get('tr.mat-row.cdk-row').contains('Cypress-2').get('td.mat-cell.cdk-cell.cdk-column-score.mat-column-score').should('have.value', '');
    });
    it("should verify perfect submit", () => {
        cy.visit('/leaderboard');
        cy.get('tr.mat-row.cdk-row').contains('Cypress-Perfect').get('td.mat-cell.cdk-cell.cdk-column-score.mat-column-score').should('have.value', '');
    });
});