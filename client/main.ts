import express = require("express");
import axios from "axios";

const app = express();
const port = 8080;

app.get("/", (req, res) => {
  axios
    .get("http://server:3000")
    .then((res) => console.log(res.data))
    .catch((err) => console.error(err));
  res.send("Hello from client!");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
