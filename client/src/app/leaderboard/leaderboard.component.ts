import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

  data: any;
  displayedColumns: string[] = ['position', 'name', 'score'];
  created:any = {};

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.http.get('http://localhost:3000/leaderboard/').subscribe(
      res => {
        this.data = res;
        console.log(this.data);
      },
      err => {
        console.log("An error occured :", err.message);
        this.router.navigate(['']);
      });
    this.created = this.router.routerState.root.queryParams['value'];
    console.log(this.created);
  }

}
