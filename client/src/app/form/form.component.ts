import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Questions } from '../../assets/questions';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true},
    //provide: , useValue: { color: 'accent' },
  }]
})
export class FormComponent implements OnInit {

  myFormGroup: FormGroup[] = [];
  nameGroup: FormGroup;
  public questions = Questions;
  answer: any[] = [];

  constructor(
    private _formBuilder: FormBuilder,
    private http: HttpClient,
    private router: Router) { }

  ngOnInit(): void {
    this.questions.forEach((value, index) => this.myFormGroup[index] = this._formBuilder.group({Ctrl0: ['', Validators.required]}))
    this.nameGroup = new FormGroup({name: new FormControl()});
  }

  onSubmit() {
    this.questions.forEach((value, index) => this.answer[index] = this.myFormGroup[index].value['Ctrl0']);
    let score = this.calculate();
    this.http.post('http://localhost:3000/submit/', {name: this.nameGroup.value['name'], score: score})
    .subscribe(
      res => {
        this.router.navigate(['leaderboard'], {queryParams: {name: res['name'], score: res['score'], created: res['createdAt'].split('.')[0]}});
      },
      err => {
        console.log("An error occured :", err.message);
        this.router.navigate(['']);
      });
  }

  calculate() {
    let score = 0;
    this.answer.forEach((value, index) => {if (value == this.questions[index]['response']) {score+=1;}});
    return score;
  }

}
