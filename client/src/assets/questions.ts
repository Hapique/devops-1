export const Questions = [
    {
        questions: 'Supposons qu\'après-demain c\'est dimanche. Quel jour serions-nous le lendemain d\'avant-hier ?',
        answers: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi'],
        response: 'Jeudi'
    },
    {
        questions: 'Chez les animaux, aucune femelle ne s\'appelle...',
        answers: ['La paonne', 'La levrette', 'La kangouroute', 'La hérisonne'],
        response: 'La kangouroute'
    },
    {
        questions: 'La statue de la liberté porte...',
        answers: ['Une ceinture', 'Des boucles d\'oreilles', 'Une bague', 'Des sandales'],
        response: 'Des sandales'
    },
    {
        questions: 'Dans un salon, vous ne pourrez pas vous asseoir confortablement sur...',
        answers: ['Un crapaud', 'Une bergère', 'Un confident', 'Une courgette'],
        response: 'Une courgette'
    },
    {
        questions: 'Qu\'est-ce qui ne sert pas à faire la cuisine ?',
        answers: ['Un piano', 'Une guitare', 'Une mandoline', 'Un violon'],
        response: 'Un violon'
    },
    {
        questions: 'Le magenta est une couleur rose violacé qu\'on appelle aussi...',
        answers: ['Fuschia', 'Fushia', 'Fuschsia', 'Fuchsia'],
        response: 'Fuchsia'
    },
    {
        questions: 'Lors d\'une course, si vous doublez le 4e et que le 1er est disqualifié, vous vous trouvez alors à la...',
        answers: ['1ere place', '2e place', '3e place', '4e place'],
        response: '3e place'
    },
    {
        questions: 'Parmis ces noms de divinité, un seul est inventé. Lequel ?',
        answers: ['Thalassa', 'Hypnos', 'Niké', 'Synthétos'],
        response: 'Synthétos'
    },
]